import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import { getPage, postComment } from '../../../redux/actions/BlogActions'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import { HashLoader } from 'react-spinners'
import ImageBlock from '../../components/blogFrameworks/imageComponents/ImageBlock'
import BlogDescription from '../../components/blogFrameworks/contentComponents/BlogDescription'
import Share from '../../components/blogFrameworks/contentComponents/Share'
import CommentsForm from '../../components/blogFrameworks/commentsComponents/CommentsForm'
import CommentsList from '../../components/blogFrameworks/commentsComponents/CommentsList'
import DefaultImg from '../../../images/DefaultImg.jpg'
import SideContainer from "../SideContainer/index"
import Header from '../../components/Header'

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        { getPage, postComment }, dispatch
    )
}

const mapStateToProps = (state, ownProps) => {
    const { articles, fetched } =  state.BlogReducer
    let blogItem;
    articles && articles.forEach(item => {
        if(ownProps.match.params._id === item._id) blogItem = {...item }
    })
    return { blogItem, fetched }
}

const ASSETS = {
    LANG: {
        edit: "Edit blog"
    },
    SPINNER_COLOR: "#36D7B7"
}

@connect(mapStateToProps, mapDispatchToProps)
export default class BlogItem extends PureComponent {
    static defaultProps = {
        blogItem: {},
        postComment: ()=>{},
        getPage: ()=>{}
    }

    static propTypes = {
        blogItem: PropTypes.object,
        getPage: PropTypes.func,
        postComment: PropTypes.func
    }

    componentDidMount() {
        const { fetched, getPage, match} = this.props
        if (!fetched) getPage(match.params.page)
    }

    sendComment = ( params ) => {
        const { blogItem, postComment } = this.props
        const { nameInput, emailInput, areaInput } = params
        const { _id  } = blogItem
        const postData = { 
            articleId: _id,
            text: areaInput,
            author: emailInput,
            authorName: nameInput
        }
        postComment(postData);
    }

    render() {
       const { blogItem, match } = this.props
       const { page } = match.params
       const { _id, title, body,  created, image, comments } = blogItem
       const blogImage = image ? image : DefaultImg
       const commentsCount = comments ? comments.length : 0
        if (!_id) return <div><HashLoader color={ASSETS.SPINNER_COLOR} className="spinner" /></div>
       return [
            <Header key="header" section='blog' link={`/${page}/edit/${_id}`} linkName={ASSETS.LANG.edit} />,
            <div key="blogsContent" className="blogsWrapper">
                 <div className="blogsContent" >
                      <ImageBlock image={ blogImage } created={ created } singleBlog={true} commentsCount={commentsCount}/>
                      <BlogDescription title={ title } body={ body } />
                      <Share />
                      <CommentsList comments={ comments } />
                      <CommentsForm sendComment={this.sendComment} />
                 </div>
                 <SideContainer page={page} />
            </div>
       ]
    }
} 