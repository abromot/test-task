import React, { PureComponent } from "react";


export default class PageNotFound extends PureComponent {
	render() {
		return <section>[ PAGE NOT FOUND ]</section>
	}
}
