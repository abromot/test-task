import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { createBlog, getPage } from '../../../redux/actions/BlogActions'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import FieldForCreate from '../../components/blogFrameworks/contentComponents/FieldForCreate'
import SideContainer from "../SideContainer/index"
import Header from '../../components/Header'

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        { createBlog, getPage }, dispatch
    )
}

const mapStateToProps = state => {
    const { articles, fetched } = state.BlogReducer
    return { articles, fetched }
}

const ASSETS = {
    INDEX_PAGE: 1,
    LANG: {
        created: "Blog was created!",
        notValid: "All fields are required!"
    }
}


@connect(mapStateToProps, mapDispatchToProps)
export default class CreateBlog extends PureComponent {
    static defaultProps = {
        articles: [],
        createBlog: ()=>{}
    }

    static propTypes = {
        _id: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        createBlog: PropTypes.func
    }
    
    state = {
        created: false,
        notValidation: null
    }

    componentDidMount() {
        const { fetched, getPage} = this.props
        if (!fetched) getPage(ASSETS.INDEX_PAGE)
    }

    saveChanges = (data) => {
        const { title, body } = data
        const { createBlog } = this.props
        if (!title || !body ) {
            this.setState({notValidation: true})
        } else {
            createBlog(data)
            this.setState({created:true, notValidation:false})
        }
    }

    render() {
        const { created, notValidation } = this.state
        return [
                <Header key="header" section='blog' />,
                <div key="blogsContent" className="blogsWrapper">
                    <div className="blogsContent" >
                        { created? <h1 key="title">{ASSETS.LANG.created}</h1> : <FieldForCreate getParams={this.saveChanges} /> }
                        { notValidation && <p className="notValid">{ASSETS.LANG.notValid}</p> }
                    </div>
                    <SideContainer page={ASSETS.INDEX_PAGE}/>
                </div>
        ]
    }
}