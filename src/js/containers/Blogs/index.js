import React, { PureComponent, Component } from "react"
import { connect } from 'react-redux'
import { getPage } from '../../../redux/actions/BlogActions'
import { bindActionCreators } from 'redux'
import MapingBlog from '../../components/MapingBlog'
import Paginator from '../../components/Paginator'
import PropTypes from 'prop-types';
import SideContainer from "../SideContainer/index"
import Header from '../../components/Header'
import { HashLoader } from 'react-spinners'


const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        { getPage }, dispatch
    )
}

const mapStateToProps = state => {
    const { BlogReducer } = state
    const { articles, pagesCount } = BlogReducer
    return {
        articles,
        pagesCount
    }
}

const ASSETS = {
    LANG: {
        createNew: "Create New"
    },
    SPINNER_COLOR: "#36D7B7"
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Blogs extends Component {
    static defaultProps = {
        blogItem: {},
        getPage: ()=>{},
        articles: [],
        pagesCount: 0
    }

    static propTypes = {
        blogItem: PropTypes.object,
        getPage: PropTypes.func,
        articles: PropTypes.array,
        pagesCount: PropTypes.number
    }

    componentDidMount() {
        const { getPage, match } = this.props
        const { params } = match
        getPage(params.page)
    }

    componentWillReceiveProps(nextProps) {
        const page = nextProps.match.params.page
        const oldPage = this.props.match.params.page
        if (page !== oldPage) this.props.getPage(page)
    }

    renderAllBlogs = (data) => {
        const { articles, page } = data
        if (!articles) return
        return articles.map((item, index) => <MapingBlog page={page} key={index} data={item}/>)
    }

    render() {
        const { pagesCount, getPage, articles, match: { params: { page } } } = this.props

        if (!pagesCount) return <div><HashLoader color={ASSETS.SPINNER_COLOR} className="spinner" /></div>
        return ([
            <Header key="header" section='blog' link="/create" linkName={ASSETS.LANG.createNew} />,
            <div key="blogsContent" className="blogsWrapper">
                <div className="blogsContent" >
                    {this.renderAllBlogs({articles, page})}
                </div>
                <SideContainer page={page} />
            </div>,
            <Paginator
                key="paginator"
                getPage={getPage}
                pagesCount={pagesCount}
                currentPage={Number(page)}
            />
        ])
    } 
}