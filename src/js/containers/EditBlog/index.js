import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import { putBlogBody, getPage } from '../../../redux/actions/BlogActions'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import { Editor } from '@tinymce/tinymce-react'
import { EDITOR_INIT } from "../../constants/index"
import SideContainer from "../SideContainer/index"
import { Link } from 'react-router-dom'
import Header from '../../components/Header'

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        { putBlogBody, getPage }, dispatch
    )
}

const mapStateToProps = (state, ownProps) => {
    const { articles, fetched } =  state.BlogReducer
    let blogItem = {}
    articles && articles.forEach(item => {
        if(ownProps.match.params._id === item._id)  blogItem = {...item }
    })
    return { blogItem, fetched}
}

const ASSETS = {
    LANG: {
        linkToBlock: "View changes",
        save: "Save Changes",
        newTitle: "Enter new title:",
        newBody: "Enter new body",
        notValid: "All fields are required!",
        alreadySubmit: "Changes successfully saved!",
    }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class EditBlog extends PureComponent {
    static defaultProps = {
        blogItem: {},
        putBlogBody: ()=>{}
    }

    static propTypes = {
        blogItem: PropTypes.object,
        putBlogBody: PropTypes.func
    }
    
    constructor(props) {
        super(props)
        this.state={
            title: props.blogItem.title,
            body:  props.blogItem.body,
            validation: true,
            alreadySubmit: false
        }
    }

    componentDidMount() {
        const { fetched, getPage, match} = this.props
        if (!fetched) getPage(match.params.page)
    }

    componentWillReceiveProps(newProps) {
           const { blogItem } = newProps
           const { body, title } = blogItem
           this.setState({
                title,
                body
           })
    }

    changeBody = (event) => {
        const value  = event.target.getContent()
        this.setState({body: value, validation: true})
    }

    changeTitle = (event) => {
        const {value} = event.target
        this.setState({title: value, validation: true})
    }

    saveChanges = () => {
        const { putBlogBody, getPage, match } = this.props;
        const { body, title } = this.state
        if ( body && title ) {
            const {_id} = match.params
            const dataToPut = {
                body,
                _id,
                title
            }
            putBlogBody(dataToPut)
            this.setState({alreadySubmit: true})
        } else {
            this.setState({validation: false})
        }
    }

    renderEditor = (title, body, validation) => (
        <div className="blogsContent fieldForCreate" >
            <h2>{ASSETS.LANG.newTitle}</h2>
            <input value={title} onChange={this.changeTitle}/>
            <h2>{ASSETS.LANG.newBody}</h2>
            <Editor value={body} init={ EDITOR_INIT } onChange={this.changeBody} />
            <button className="simpleButton" onClick={this.saveChanges}>{ASSETS.LANG.save}</button>
            { !validation && <p className="notValid">{ASSETS.LANG.notValid}</p> }
        </div>
    )

    renderSuccess = () => {
        const { match } = this.props;
        const {page, _id} = match.params
        return (
            <div className="blogsContent fieldForCreate">
                <div className="alreadySubmit">
                    <h1 >{ASSETS.LANG.alreadySubmit}</h1>
                    <Link to={`/${page}/blog/${_id}`}>{ASSETS.LANG.linkToBlock}</Link>
                </div>
            </div>
        )
    }

    render() {
        const { title, body, validation, alreadySubmit } = this.state
        const { match } = this.props;
        const { page } = match.params
        const component = alreadySubmit ? this.renderSuccess() : this.renderEditor(title, body, validation)
        return [
            <Header key="header" section='blog' />,
            <div key="blogsContent" className="blogsWrapper">
                { component }
                <SideContainer page={page} />
           </div>
        ]
    }
}