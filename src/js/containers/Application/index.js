import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Footer from "../../components/Footer";
import { ROUTES_MAP } from "../../constants/RouterMap";
import "../../../styles/main.scss";
import Navigation from "../../components/Navigation"


export default class Application extends Component {
	
    renderAllRoutes = () => {
        return ROUTES_MAP.map((item, index) => <Route exact path={item.path}  component={item.component} key={index} />)
	}

 	render() {
		const { renderAllRoutes } = this;
		return 	(
			<div className="application">
				<Navigation />
				<div className="main">
					<Switch>
						<Redirect exact from="/" to="/1" />
						{renderAllRoutes() }
					</Switch>
				</div>
				<Footer/>
			</div>
		)
	}
}

