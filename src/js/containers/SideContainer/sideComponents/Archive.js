import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import { DateParser } from "../../../utils/DateParser"
import { MONTH_NAME } from '../../../constants/index'
import Moment from 'moment'
import { Link } from 'react-router-dom'
import FontAwesome from 'react-fontawesome'


export default class Archive extends PureComponent {
    static propTypes = {
        articles: PropTypes.array
    }

    state = {
        showId: ''
    }

    monthList = (articles) => {
        const months = JSON.parse(JSON.stringify(MONTH_NAME));
        articles.map(item=> {
            const { created, title, _id } = item
            const validDate = DateParser(created)
            let currentMonth = Moment(validDate).format("MMMM")
            months[currentMonth].push({title, _id})
        })
        return months
    }

    renderArchive = (data=[]) => {
        const { page } = this.props
        const months = this.monthList(data)
        const { showId } = this.state
        let component = []
        for (let key in months) {
            if (!months[key].length) {
                component.push(
                    <div key={key} className="monthItem">
                        <FontAwesome name="calendar" />
                        <span className="calendarName" >{key}</span>
                    </div>
                )
            } else {
                let children = months[key].map((item, index) =>
                    <li key={index} className="linkMonthItem">
                        <Link to={`/${page}/blog/${item._id}`} key={index}>
                            {item.title}
                        </Link>
                    </li>
                )
                component.push(
                    <ul key={key} className="monthItem" >
                        <FontAwesome name="calendar" />
                        <span className="calendarName">{key}</span>
                        <FontAwesome onClick={this.showLinks} id={key} name={showId === key ? 'angle-up' : 'angle-down'} className="arrow"/>
                        {showId === key && children}
                    </ul>
                )
            }
        }
        return component
    }

    showLinks = (event) => {
        const { id } = event.target
        const { showId } = this.state
        this.setState({
            showId: id === showId ? '' : id
        })
    }

    render() {
        const { articles } = this.props
        return (
            <div  className="popularLink">
                {this.renderArchive(articles)}
            </div>
        )
    }

}