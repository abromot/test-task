import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'

const ASSETS = {
    MAX_TEXT_LENGTH: 50
}

export default class Popular extends PureComponent {
    static propTypes = {
        _id: PropTypes.string,
        title: PropTypes.string,
        text: PropTypes.string,
    }

    render() {
        const { text, _id, title, page } = this.props
        const sliceText = text.slice(0, 15)
        return (
            <div key={_id} className="popularLink">
                <Link to={`/${page}/blog/${_id}`}>{title}</Link>
                <p>{sliceText}</p>
            </div>
        )
    }

}