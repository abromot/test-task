import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import Popular from "./sideComponents/Popular"
import Archive from "./sideComponents/Archive"
import FontAwesome from 'react-fontawesome'
import { Link } from 'react-router-dom'

const mapStateToProps = state => {
    const { BlogReducer } = state
    const { articles, fetched } = BlogReducer
    return {
        articles,
        fetched
    }
}

const ASSETS = {
    ARTICLES_COUNT_TO_SHOW: 2,
    LANG: {
        contact: "Contact us today for more information."
    }
}

@connect(mapStateToProps, null)
export default class SideContainer extends PureComponent {
    static defaultProps = {
        articles: []
    }

    static propTypes = {
        articles: PropTypes.array,
        page: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
    }

    renderPopular = (data) => {
        if (!data) return
        const { page } = this.props
        const popularArticles = data.sort((a, b)=> a.comments.length < b.comments.length ).slice(0, ASSETS.ARTICLES_COUNT_TO_SHOW)
        return popularArticles.map(( item, index ) => <Popular  key={index} page={page} title={item.title} _id={item._id} text={item.label} />)
    }

    render() {
        const { articles, page } = this.props

        return (
            <div className="sideContainer">
                <h1><Link to="/contact"> {ASSETS.LANG.contact}<FontAwesome name="play" /></Link></h1>
                <div className="sideSupContainers">
                    <div className="popularHead"> <FontAwesome name="star" /> Popular</div>
                    {this.renderPopular(articles)}
                </div>
                <div className="sideSupContainers">
                    <div className="popularHead"> <FontAwesome name="archive" /> Archive</div>
                   <Archive articles={articles} page={page}/>
                </div>
            </div>
        )
    }
}