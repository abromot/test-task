import React, {PureComponent} from 'react'
import NavItem from './Links'
import {Link} from 'react-router-dom'
import { NAV_LINKS } from './../../constants'
import LogoImg from '../../../images/logo.png'
import FontAwesome from 'react-fontawesome'

export default class Navigation extends PureComponent{
    
    state = {
        isOpened: false
    }
    
    renderNavigation = () => NAV_LINKS.map((item, index) => <NavItem key={index} linkTo={item.link} value={item.value} /> )
    
    showHide = () => {
        this.setState({
            isOpened: !this.state.isOpened
        })
    }

    render = () => {
        return (
            <div>
                <div className="navigation">
                    <Link className='navLogo' to='/1'>
                        <img src={LogoImg} />
                    </Link>
                    <div className='navList'>
                        { this.renderNavigation() }
                    </div>
                    <div className='expand'>
                        <FontAwesome name='align-justify' onClick={this.showHide} />
                        <div className="dropDownWrapper" onClick={this.showHide}>
                            {this.state.isOpened && this.renderNavigation() }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    
}

