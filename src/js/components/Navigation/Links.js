import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

export default class NavItem extends PureComponent{
    static propTypes = {
	     linkTo: PropTypes.string,
    	 value: PropTypes.string
    }
    render(){
        const { linkTo, value } = this.props
        return (
            <NavLink className='navItem' to={linkTo} >
                {value}
            </NavLink>
        )
    }
}

