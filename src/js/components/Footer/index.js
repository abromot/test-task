import React, { PureComponent } from "react";
import { Link } from 'react-router-dom'

const ASSETS = {
	LANG: {
		blog: "@Windsor Publishing Inc. 2016",
		contact: "Contact Us"
	}
}

export default class Footer extends PureComponent {
	render = () => <footer className="Footer">
						<div className="footerWrapper">
								<span >{ASSETS.LANG.blog}</span>
								<Link to='/contact'>{ASSETS.LANG.contact}</Link>
						</div>
					</footer>
}