import React, { PureComponent } from 'react'
import Header from '../Header'

export default class Services extends PureComponent{

    render() {
        return [
                <Header key="header" section='services' />,
                <div key="text" className='blogsWrapper'>
                    <div>
                        Morbi dignissim facilisis aliquet. Ut mollis odio enim, id gravida ipsum volutpat eu. In tristique nulla massa, vel vestibulum mi vehicula in.
                    </div>
                </div>
           ]
    }
}
