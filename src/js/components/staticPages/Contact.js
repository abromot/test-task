import React, { PureComponent } from 'react'
import Header from '../Header'

export default class Contact extends PureComponent{

    render() {
        return [
                <Header key="header" section='contact us' />,
                <div key="text" className='blogsWrapper'>
                    <div>
                        Vivamus neque metus, scelerisque vel auctor nec, ullamcorper sed tortor. Integer id euismod arcu, consectetur ultrices tellus. Proin sed orci facilisis sapien dapibus aliquam. Curabitur nec tempor sapien, eu laoreet ipsum. Mauris et elit turpis. Integer finibus condimentum bibendum. Nullam euismod tincidunt nunc, quis lacinia leo volutpat non.
                    </div>
                </div>
            ]
    }
}
