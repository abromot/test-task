import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome'

const ASSETS = {
    TAGS: ['post', 'get', 'front', 'tegs', 'text', 'picture', 'some' ]
}

export default class ImageFooter extends PureComponent{
    static propTypes = {
        created: PropTypes.string,
        image: PropTypes.string,
        tags: PropTypes.array,
        commentsCount: PropTypes.number
    }

    render() {
        const { tags, commentsCount } = this.props
        const renderTags = tags ? tags.map(item=><span key={item}> {item} </span>) : ASSETS.TAGS.map(item=><span key={item}> {item} </span>)
        return (
         <div className='imageFooter'>
            <div><FontAwesome name='tags' /> {renderTags} </div>
            <div className="comment-eye">
                <FontAwesome name='comment' /> ({commentsCount}) 
                <FontAwesome name='eye' /> ({421}) 
            </div>
         </div>
        )
    }
}