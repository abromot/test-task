import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import RotetedDate from './RotetedDate'
import ImageFooter from './ImageFooter'

export default class ImageBlock extends PureComponent{
    static propTypes = {
        created: PropTypes.string,
        image: PropTypes.string,
        singleBlog: PropTypes.bool,
        commentsCount: PropTypes.number
    }

    render() {
      const { created, image, singleBlog, commentsCount } = this.props
      const classNames = singleBlog ? 'dateWrapper' : 'dateWrapper single'
        return (
         <div className='imageWrapper'>
             <img src={ image }/>
             <RotetedDate date={created} classNames={classNames} />
             { singleBlog && <ImageFooter commentsCount={commentsCount} /> }
         </div>
        )
    }
}