import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment'
import { DateParser } from '../../../utils/DateParser'

export default class RotetedDate extends PureComponent {
    static propTypes = {
        date: PropTypes.string,
        classNames: PropTypes.string
    }

    render() {
        const { date, classNames } = this.props;
        const valideDate = DateParser(date)
        const currentDate = Moment(valideDate).format('DD')
        const monthYearDate = Moment(valideDate).format('MMMM YYYY')
        const dayWeek = Moment(valideDate).format('dddd')

        return (
            <div className={classNames}>
                <div className='currentDate'> {currentDate} </div>
                <div className='rotate' >
                    <div className='dayWeek'> {dayWeek} </div>
                    <div className='otherDate'> {monthYearDate} </div>
                </div>
            </div>
        )
    }
}