import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import { DateParser } from "../../../utils/DateParser"
import Moment from 'moment'
import SingleComment from './SingleComment'


export default class CommentsList extends Component {
    static propTypes = {
        comments: PropTypes.array,
    }

    renderCommentsTree = (data) => {

        if (!data ) return
        const allComments = data.map((item, index) => {
            const { replies, created, authorName, author, text } = item
            const valideDate =  DateParser(created)
            const date = Moment(valideDate).format('MMMM Do YYYY, h:mm ')
            if (replies && replies.length) {
              const  repliesMap = this.renderCommentsTree(replies)
              return  [
                        <SingleComment currentDate ={ date } authorName={ authorName || author }  text= { text } key={this.uniqueId()}/>,
                        <ul className="replies" key={index}> {repliesMap}</ul>
                      ]
            } else {
                return <SingleComment currentDate ={ date } authorName={authorName || author }  text= { text } key={index}/>
            }
        })
        return (<ul>{allComments}</ul>)
    }

    uniqueId = () => Math.random().toString(36).substr(2, 16);

    render() {
        const { comments } = this.props
        return (
         <div className='commentsTreeWrapper'>
             {this.renderCommentsTree(comments)}
         </div>
        )
    }
}