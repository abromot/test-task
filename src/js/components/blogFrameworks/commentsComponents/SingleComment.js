import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';

export default class SingleComment extends PureComponent {
    static propTypes = {
        currentDate: PropTypes.string,
        authorName: PropTypes.string,
        text: PropTypes.string,
    }

    render() {
        const { authorName, currentDate, text } = this.props
        return (
            <li className="commentItem">
                <div className="aboutUser">
                    <span className="authorName">{authorName}</span>
                    <span className="createDate">{currentDate}</span>
                </div>
                <p className="commentText">{text}</p>
            </li>
        )
    }
}

