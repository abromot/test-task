import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';


const ASSETS = {
    LANG: {
        name: "Name",
        email: "e-mail",
        notValid: "All fields are required!",
        submitComment: "Submit Comment"
    }
}

export default class CommentForm extends PureComponent{
    static propTypes = {
        sendComment: PropTypes.func
    }

    state = {
        nameInput: '',
        emailInput: '',
        areaInput:'',
        validation: null
    }
    
    changeTextField = (event) => {
        const { value, name } = event.target
        this.setState({ [name]: value, validation:true })
    }
    
    checkValidate = () => {
        const { nameInput, emailInput, areaInput, validation } = this.state 
        if ( nameInput && emailInput && areaInput) {
            this.setState({validation: true}, ()=>this.submitForm() )
        } else this.setState({validation: false});
        
    }

    submitForm = () => {
        const { nameInput, emailInput, areaInput } = this.state
        const { sendComment } = this.props;
        sendComment( { nameInput, emailInput, areaInput } )
        this.setState({
            nameInput: '',
            emailInput: '',
            areaInput: ''
        })
    }

    render() {
       const { nameInput, emailInput, areaInput, validation } = this.state 
        return (
         <div className="commentFormWrapper" >
             <div className="groupInput">
                 <input className="name-input" name="nameInput" placeholder={ASSETS.LANG.name} onChange={this.changeTextField} value={nameInput}/>
                 <input className="email-input" name="emailInput" placeholder={ASSETS.LANG.email} onChange={this.changeTextField} value={emailInput}/>
             </div>
             <textarea className="text-area" name="areaInput" value={areaInput} onChange={this.changeTextField}></textarea>
             <button onClick={this.checkValidate} >{ASSETS.LANG.submitComment}</button>
             { validation !== null && !validation && <p className="notValid">{ASSETS.LANG.notValid}</p> }
         </div>
        )
    }
}