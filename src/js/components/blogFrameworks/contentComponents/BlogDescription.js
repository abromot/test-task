import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import renderHTML from 'react-render-html';


export default class BlogDescription extends PureComponent{
    static propTypes = {
        body: PropTypes.string,
        title: PropTypes.string
    }

    render() {
      const { body, title } = this.props
      const description = renderHTML(body) 
        return (
         <div className='descriptionWrapper'>
             <h1>{title}</h1>
             <div>{description}</div>
         </div>
        )
    }
}