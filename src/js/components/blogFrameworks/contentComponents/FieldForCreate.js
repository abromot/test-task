import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { EDITOR_INIT } from '../../../constants/index'
import { Editor } from '@tinymce/tinymce-react'

const ASSETS = {
    LANG: {
        enterTitle: "Enter title:",
        enterBody: "Enter main content:",
        save: "Save"
    }
}

export default class FieldForCreate extends PureComponent {
    static propTypes = {
        getParams: PropTypes.func,
    }

    state = {
        title: '',
        status: 'active',
        metaTitle: 'title',
        metaDescription: 'descsription',
        metaKeywords: 'keywords',
        body: '',
    }

    changeBody = (event) => {
        const value  = event.target.getContent()
        this.setState({body: value})
    }

    changeTitle = (event) => {
        const {value} = event.target
        this.setState({title: value})
    }

    saveChanges = () => {
        const dataToPost = {
            ...this.state
        }
        this.props.getParams( dataToPost )

    }

    render() {
        const { title, body } = this.state
        return (
            <div className="fieldForCreate">
                <h2 >{ASSETS.LANG.enterTitle}</h2>
                <input value={title} onChange={this.changeTitle}/>
                <h2>{ASSETS.LANG.enterBody}</h2>
                <Editor value={ body } init={ EDITOR_INIT } onChange={ this.changeBody } />
                <button className="simpleButton" onClick={this.saveChanges}>{ASSETS.LANG.save}</button>
            </div>
        )
    }
}