import React, { PureComponent } from 'react';
import FontAwesome from 'react-fontawesome'

const ASSETS = {
        SHARE_ICONS: [
            { name:'facebook-square', network: 'FACEBOOK' },
            { name:'google-plus-square', network: 'GOOGLE' },
            { name:'twitter-square', network: 'TWITTER' },
            { name: 'linkedin-square', network: 'LINKEDIN' },
            { name:'tumblr-square', network: 'TUMBLR' }
        ],
        SINGLE_ICON: {name: 'share-alt'},
        LANG: {
            share: 'share'
        }
    }

export default class Share extends PureComponent{
    shareInNetwork = (networkName) => alert(`Share in ${networkName}`)
   
    renderShareIcons = () => ASSETS.SHARE_ICONS.map((item, index) => <FontAwesome className='sharedIcon' name={item.name} onClick={()=>this.shareInNetwork(item.network)} key={index} />)

    render() {     
        return (
         <div className='shareWrapper'>
            <FontAwesome name={ASSETS.SINGLE_ICON.name} className='sharedIcon' /> {ASSETS.LANG.share}
             {this.renderShareIcons()}
         </div>
        )
    }
}