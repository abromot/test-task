import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import DefaultImg from '../../../images/DefaultImg.jpg'
import ImageBlock from '../blogFrameworks/imageComponents/ImageBlock'

const ASSETS = {
    LANG: {
        linkText: "continue reading"
    },
    CHAR_LIMIT: 250
}

export default class MapingBlog extends PureComponent {
    static propTypes = {
        data: PropTypes.object,
        page: PropTypes.string
    }

    render() {
        const { data, page } = this.props
        const {_id, title, body, created, image } = data
        const bodyText = body.replace(/<[^>]+>/g, '').slice(0, ASSETS.CHAR_LIMIT);
        const blogImage = image ? image : DefaultImg
        return(
            <div key={_id} className="contentWrapper">
                <div className="blogLeftColumn" > <ImageBlock image={ blogImage } created={ created } singleBlog={false} /></div>
                <div className="blogRightColumn" >
                    <h3>{title}</h3>
                    <p>{bodyText}...</p>
                    <Link to={`${page}/blog/${_id}`}  >{ASSETS.LANG.linkText}</Link>
                </div>
            </div>
        )
    }
} 