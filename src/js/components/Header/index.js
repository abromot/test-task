import React, { PureComponent } from "react";
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

export default class Header extends PureComponent {
    static propTypes = {
		section: PropTypes.string,
		link: PropTypes.string,
		linkName: PropTypes.string
	}
	render() {
		const { section, link, linkName } = this.props
		return (
			<header className="Header">
			   <div className="headerWrapper">
				   <Link to='/1' className="titleHead">{section}</Link>
					{ link &&  <Link to={link}>{linkName}</Link> }
			   </div>
			</header>
		)
    }
}