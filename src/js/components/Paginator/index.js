import React, {PureComponent, Component} from "react"
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'


const ASSETS = { 
    FIRST_PAGE: 1,
    SPAN_TAG_NAME: 'SPAN',
    PAGINATOR_OFFSET: 3,
    PAGINATOR_LAST_PAGE_NEIGHBORG: 2,
    SELECTED_ITEM_CLASS: 'paginatorItemSelected',
    ITEM_CLASS: 'paginatorItem'
    
}

export default class Paginator extends Component {
    static propTypes = {
        pagesCount: PropTypes.number,
        currentPage: PropTypes.number
    }

    renderPagination = () => {
        const { pagesCount, currentPage } = this.props
        let pages = [];
        if ( pagesCount <= ASSETS.PAGINATOR_OFFSET) {
            for (let i = ASSETS.FIRST_PAGE; i <= pagesCount; i++ ) {
                pages.push(<Link to = {`/${i}`}   className={ currentPage === i ? ASSETS.SELECTED_ITEM_CLASS : ASSETS.ITEM_CLASS }  key={i} >{i}</Link>)
            }
        } else if (currentPage === pagesCount) {
            let currentPageWithNeighbors = currentPage - ASSETS.PAGINATOR_LAST_PAGE_NEIGHBORG
            for (let i = currentPageWithNeighbors; i < pagesCount; i++ ) {
                pages.push(<Link to = {`/${i}`} className={ currentPage === i ? ASSETS.SELECTED_ITEM_CLASS : ASSETS.ITEM_CLASS }  key={i} >{i}</Link>)
                 if (pages.length === ASSETS.PAGINATOR_OFFSET) break
            }
            pages.push(<Link to = {`/${pagesCount}`} key={pagesCount} className={ currentPage === pagesCount ? ASSETS.SELECTED_ITEM_CLASS :  ASSETS.ITEM_CLASS }>{pagesCount}</Link>)
        } else {
            let currentPageWithNeighbors = currentPage-1 === 0 ? currentPage : currentPage-1
            for (let i = currentPageWithNeighbors; i < pagesCount; i++ ) {
                pages.push(<Link to = {`/${i}`} className={ currentPage === i ? ASSETS.SELECTED_ITEM_CLASS :  ASSETS.ITEM_CLASS }  key={i} >{i}</Link>)
                if (pages.length === ASSETS.PAGINATOR_OFFSET) break
            }
            pages.push(" ... ")
            pages.push(<Link to = {`/${pagesCount}`} key={pagesCount} className={ currentPage === pagesCount ? ASSETS.SELECTED_ITEM_CLASS : ASSETS.ITEM_CLASS } >{pagesCount}</Link>)
        }
        return (<div className="paginatorWrapper" key = "paginatorWrapper" >{pages}</div>)
    }

    render() {
        return (
            this.renderPagination()
        )  
    }

}