import Blogs from "../containers/Blogs"
import BlogItem from '../containers/BlogItem'
import EditBlog from '../containers/EditBlog'
import CreateBlog from '../containers/CreateBlog'
import AboutUs from '../components/staticPages/AboutUs'
import Services from '../components/staticPages/Services'
import Guarantee from '../components/staticPages/Guarantee'
import Resources from '../components/staticPages/Resources'
import Forms from '../components/staticPages/Forms'
import Contact from '../components/staticPages/Contact'
import PageNotFound from '../containers/PageNotFound'

export const ROUTES_MAP = [
    {path: '/aboutUs', component: AboutUs },
    {path: '/services', component: Services },
    {path: '/guarantee', component: Guarantee },
    {path: '/resources', component: Resources },
    {path: '/forms', component: Forms },
    {path: '/contact', component: Contact },
    {path: '/create', component: CreateBlog },
    {path: '/:page', component: Blogs },
    {path: '/:page/blog/:_id', component: BlogItem },
    {path: '/:page/edit/:_id', component: EditBlog },
    {path: '*', component: PageNotFound }
]