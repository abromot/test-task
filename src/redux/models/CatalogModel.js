import { LIST_ITEMS_LIMIT } from "../../js/constants" ;
const CatalogModel = {
	error: null,
	fetched: false,
	isFetching: false,
	data: [],
	total: null,
	offset: 0,
	limit: LIST_ITEMS_LIMIT,
	isLoadingComplete: false
}

export default CatalogModel;