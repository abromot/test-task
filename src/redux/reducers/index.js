import { combineReducers } from 'redux';
import BlogReducer from './BlogReducer';

const rootReducer = combineReducers({
    BlogReducer
})

export default rootReducer