import BlogsModel from '../models/BlogsModel'


export default function (state = BlogsModel, action) {
    const { type, payload } = action;

    switch (type) {
	case 'GET_PAGE':
        return {  ...state, ...payload, fetched: true };
    break;

    case 'UPDATE_BLOG_COMMENT':
        const  { articleId } = payload
        state.articles.filter(item=> item._id === articleId).shift().comments.push(payload)
        return { ...state }
    break;

    case 'UPDATE_BLOG_BODY':
        //api does not give a comment field in response
        const { _id } = payload
        state.articles.forEach((item, index, arr) => {
           if(item._id === _id) {
               payload.comments = item.comments;
               arr.splice(index, 1, payload)
           }
        })
        return {...state }
    break;

    case 'CREATE_BLOG':
        //api does not give a comment field in response
        payload.comments = []
        state.articles.push(payload)
        return {...state}
    break;

    default: return { ...state};
    }
}