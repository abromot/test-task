import { GET, POST, PUT } from '../../js/constants'

export const getPage = (page=1) => async dispatch => {
    let result, resultBody;
    try {
        result = await fetch(`http://api.blog.testing.singree.com/?page=${page}`, GET)
        resultBody = await result.json()
    } catch(err) {
        throw new Error(err)
        return
    }

    dispatch({type: 'GET_PAGE', payload:resultBody})
}

export const postComment = (data) => async dispatch => {
    let result, resultBody
    const jsonData = JSON.stringify(data)
    try {
        result = await fetch(`http://api.blog.testing.singree.com/comment/`, { ...POST, body:jsonData })
        resultBody = await result.json()
    } catch(err) {
        throw new Error(err)
        return
    }

    dispatch({type: 'UPDATE_BLOG_COMMENT', payload: resultBody})
}

export const putBlogBody = (data) => async dispatch => {
    let result, resultBody
    const { _id, body, title } = data
    const jsonData = JSON.stringify({body, title})
    try {
        result = await fetch(`http://api.blog.testing.singree.com/${_id}`, { ...PUT, body:jsonData })
        resultBody = await result.json()
    } catch(err) {
        throw new Error(err)
        return
    }

    dispatch({type: 'UPDATE_BLOG_BODY', payload:  resultBody })
}


export const createBlog = (data) => async dispatch => {
    let result, resultBody
    const jsonData = JSON.stringify(data)
    try {
        result = await fetch(`http://api.blog.testing.singree.com/`, { ...POST, body:jsonData })
        resultBody = await result.json()
    } catch(err) {
        throw new Error(err)
        return
    }
    dispatch({type: 'CREATE_BLOG', payload: resultBody})
}



